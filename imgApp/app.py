from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
app.debug = True

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://testuser:testpass@localhost/testdb'
app.config['UPLOAD_FOLDER'] = 'uploads/'

db = SQLAlchemy(app)

migrate = Migrate(app, db)

from views import *

if __name__ == '__main__':
    app.run()
