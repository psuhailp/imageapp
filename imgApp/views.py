from flask import request, jsonify, abort, make_response
from app import app, db
from models import FileContents
import uuid, os

@app.route('/')
def index():
    return "<h1>Home Page</h1>"

@app.route('/upload/', methods=['POST'])
def upload_img():

    if request.method == 'POST':
        file = request.files['pic']
        extension = os.path.splitext(file.filename)[1]
        f_name = str(uuid.uuid4()) + extension
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], f_name))
        imgfile = FileContents(name=file.filename, data=os.path.join(app.config['UPLOAD_FOLDER'], f_name))

        db.session.add(imgfile)
        db.session.commit()

        return file.filename

    else:
        abort(404)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)
