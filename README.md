# README #

Very basic API application for uploading image using Flask

### working ###

* run 

		python app.py

* run 

		curl -i -H "Content-Type: multipart/form-data" -F "pic=@IMAGE_FILE_PATH" -X POST http://127.0.0.1:5000/upload/

* posgresql
		- user=testuser
		- pass=testpass
		- db=testdb

* images uploaded to 'uploads' folder

## DONE ##
* Environment setup
* Learning basic flask usage
* Learning basic postgresql, db-migration, sqlalchemy etc

## TO DO ##
* writing tests
* Application structure
* proper validation
* flask-restful